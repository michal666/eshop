using System;

namespace EShop
{
    class Order
    {
        public int OrderId { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
        public decimal Price { get; set; }
        public int Quantity {get;set;}
        public string Adress { get; set; }

    }

}